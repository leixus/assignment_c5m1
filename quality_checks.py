import re

def duplicated_row_numbers(df):
    '''
    Counts the number of duplicated rows of a dataframe.

    Args:
        df (pandas.DataFrame): The dataframe to be inspected.
    
    Returns:
        int: The number of duplicated rows in the dataframe.
    '''
    return df.duplicated().sum()

def missing_counts(df):
    '''
    Counts the number of missing values per column in a dataframe.

    Args:
        df (pandas.DataFrame): The dataframe to be inspected.
    
    Returns:
        pandas.Series: The number of missing values (int) per column of the dataframe, only columns with missing values (>0) are shown.
    '''
    s = len(df)-df.count()
    return s[s>0]

def truck_id_maintenances(df_1, df_2, col):
    '''
    Generates a list of unique strings that are present in the first dataframe but not the second dataframe.

    Args:
        df_1 (pandas.DataFrame): The first dataframe.
        df_2 (pandas.DataFrame): The second dataframe.
        col (string): The common column name in both dataframe to do the comparison
    
    Returns:
        list: The list of unique values in the common column presented in the first dataframe
    '''
    return list(df_1[col][~df_1[col].isin(df_2[col])].unique())

def email_check(em):
    '''
    Checks if an email address is in correct format.

    Args:
        em (string): The email address string to be checked.

    Returns:
        boolean
    '''
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
    return bool(re.fullmatch(regex, em))