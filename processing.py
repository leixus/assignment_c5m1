import pandas as pd
from quality_checks import email_check
from datetime import datetime

def merge_df(df1, df2, col):
    '''
    Merges two dataframe based on a common.

    Args:
        df1 (pandas.DataFrame): The first dataframe.
        df2 (pandas.DataFrame): The second dataframe.
        col (string): The common column both dataframe shares to be merged on.
    
    Returns:
        pandas.DataFrame: A merged dataframe.
    '''
    return pd.merge(df1, df2, on=col)

def stdize(t):
    '''
    Converts and standardize a string to lowercase and remove whitespaces.

    Args:
        t (string): The string to be converted.

    Returns:
        string: Each string with lowercase and no whitespaces.
    '''
    return str(t).lower().replace(' ', '')

def one_hot_encoding(df, col):
    '''
    Performs One-Hot encoding for a dataframe based on specified column(s).

    Args:
        df (pandas.DataFrame): The dataframe to perform One-Hot encoding.
        col (list): The column name in the dataframe to be encoded.

    Returns:
        pandas.DataFrame: An encoded dataframe with corresponding added column(s).
    '''
    df_encoded = pd.get_dummies(df, columns=col)
    return df_encoded

def email_validity(e):
    '''
    Checks the string if the format is a valid email address format.

    Args:
        e (string): The string to be checked.
    
    Returns:
        boolean: Indicating if the corresponding string is a valid email address format.
    '''
    return email_check(str(e))

def date_format(dates):
    '''
    Verifies if the input date is a valid date and is in form of 'DD-MM-YYYY'. If yes, then convert to form 'YYYY-MM-DD', otherwise set to 'NaT'.

    Args:
        dates (list): A list string (date).
    
    Returns:
        datetimeIndex: A list of formatted date.
    '''
    cor_date = []
    for item in dates:
        try:
            res = datetime.strptime(item, "%d-%m-%Y")
            cor_date.append(item)
        except:
            cor_date.append('NaT')
    return pd.to_datetime(cor_date, dayfirst=True).date

def maintenance_cost_efficiency(df):
    '''
    Calculates the cost of maintenance per kilometer using following formula:
    maintenance cost / (current mileage - mileage at last service)
    If no previous maintenance record exists, the "mileage at last service" is replaced by "initial_milage" from fleet dataset.

    Args:
        df (pandas.DataFrame): The merged dataset of "maintenance" record and "fleet" record based on common column "truck_id"
    
    Returns:
        pandas.Series: The calculated cost of maintenance per kilometer based on input dataframe
    '''
    m_cost = 'maintenance_cost'
    m_now = 'milage'
    m_init = 'initial_milage'
    df.sort_values(by=['truck_id', 'maintenance_date'], ascending = [True, False], inplace=True, ignore_index=True)
    m_cost_pkm = []
    for rn in list(range(0, len(df))):
        if (rn == len(df)-1) or (df['truck_id'][rn] != df['truck_id'][rn+1]):
            result = df[m_cost][rn] / (df[m_now][rn] - df[m_init][rn])
        else:
            result = df[m_cost][rn] / (df[m_now][rn] - df[m_now][rn+1])
        m_cost_pkm.append(round(result, 2))
    return pd.Series(m_cost_pkm)