# C5_M1_Assignment: Data Transformation and Cleaning

## Introduction
This assignment aims at developing a comprehensive data analysis pipeline using two datasets below, to provide in-depth insights into the maintenance history and requirements of the fleet for further analysis.

* `maintainence_records.csv` - a dataset with detailed maintenance logs;

* `fleet_information.csv` - a dataset with additional info about each truck in the fleet, to enrich the analysis.

## Tasks
The main pipeline file (`pipeline.py`) is written to accomplish the following tasks:

1. Load the configuration and read the provided .csv datasets.
2. Check the data quality of the datasets (e.g., duplicated records, missing data) and make comparisons between datasets. The quality check and comparison results are logged into a separate log file under the same directory: `data_quality.log`. Duplicated records are removed.
3. All data columns are inspected and converted to a consistent date format, and "service_type" column is standardized (lowercase, no whitespace).
4. Do a One-Hot encoding to the "service_type" column to facilitate analysis.
5. Validate the format of technicial email address and add a boolean column "email_valid" to indicate the validity.
6. Merge two datasets based on the common column "truck_id" and save the processed and merged dataset in a file: `fleet_maintenance_MERGED.csv`, for further processing.

## Notes
* If errors occur during configuration loading, error messages will be logged into a separate log file under the same directory: `application_errors.log`

* All mentioned quality check and data processing functions are collected in respective python module file: `processing.py`, `quality_checks.py`. These functions are associated with detailed explanation of the functionality, input/output parameters.

## How to run the pipeline
In terminal, run this pipeline script as easy as:

    python3 pipeline.py

## Personal reflections
In this session, some reflections are collected in bullt points.

* Date formatting and standardization on "service_type" column could be done before counting and removing the duplicated rows. This is because, for example, 'Repair' and 'repair' are treated as two different items instead of as duplicates, which could prevent the duplicate check from identifying the actual duplicated rows. I have done an extra trial in this assignment, to do all the formatting and standardization work before checking the duplicates. Luckily, the result is the same, so I keep the work sequence as it is now.

* Based on the pipeline, there are only 3 duplicated rows in the original fleet_information dataset. However, if take a look manually we can find that for truck_id = TRK002 on row number 2 and 16, they are also duplicated.  The applied quality check in this pipeline does not find this item, simply because the date format in "purchase_date" column is different (position of month and day). In the real environment, one would also like to fix this kind of problem to avoid this duplication. In this assignment, I did not touch on this issue, which would cause multiple records in the resulting merged dataset.

* Regarding the calculated "maintenance_cost_per_km", I can see some negative values, which should be wrong. However, these come from inaccurate original data from the maintenance record, for example, the milage decreases in a later date. From here, the work should be done to check the data source in the real working environment.

* I do not really follow the best practice of writing code from software development perspective, that is, the pipeline is not that granular, which would reduce the degree of maintenance to some extent (I have tried my best to make it easy to maintain). I hope I can improve it as my coding experience grows along the way :)