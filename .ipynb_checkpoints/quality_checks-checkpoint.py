# Function: duplicates_check
# Input: a DataFrame
# Output: int
# Description: the output is the number of duplicated rows
def duplicates_check(df):
    num_dup = df.duplicated().sum()
    return num_dup

# Function: missing_counts
# Input: a DataFrame
# Output: a Series, missing counts per column
# Description: 
###def missing_counts(df):
###    todo

# Function: truck_id_maintenance
# Input: 2 DataFrame (1 maintenance and 1 fleet)
# Output: List
# Description: the output is a list of unique truck_id that are present in the maintenance data but not the fleet data
def truck_id_maintenances(df_m, df_f):
    return list(df_m['truck_id'][~df_m['truck_id'].isin(df_f['truck_id'])].unique()) # List

# Function: email_check
# Input: an email address
# Output: Boolean
# Description: the output is a boolean if the email format is valid or not
import re
def email_check(em):
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
    return bool(re.fullmatch(regex, em))