############################
##  Author: Xiangyu Lei  ###
##  Date:   2024-04-24   ###
############################

# Import all necessary modules and packages.
import pandas as pd
import os, logging, sys

from quality_checks import duplicated_row_numbers, missing_counts, truck_id_maintenances    # Custom functions based on assignment needs
from processing import merge_df, stdize, email_validity, date_format, one_hot_encoding, maintenance_cost_efficiency

# Hide the traceback errors in log files if exists.
sys.tracebacklimit = 0

# Define the relevant filenames.
data_source_file = 'data_sources.json'
data_quality_log_file = 'data_quality.log'
application_errors_log_file = 'application_errors.log'
result_merged_file = 'fleet_maintenance_MERGED.csv'

# Delete the log files if exist.
file_to_delete = [data_quality_log_file, result_merged_file]
for filename in file_to_delete:
    if os.path.isfile(filename):
        os.remove(filename)

# Data loading from the source file ("data_sources.json") defined in "data_source_file".
# If any file is missing, the error message will be logged into a log file ("application_errors.log") defined in "application_errors_log_file"
# and script terminates. All these are accomplished by below try-catch block.
try:
    data_source = pd.read_json(data_source_file)
    data_source.set_index('type', inplace=True)     # This is to faciliate indexing on correct file in the next two lines
    data_maintenance = pd.read_csv(data_source['path']['maintenance'])
    data_fleet = pd.read_csv(data_source['path']['fleet'])
except Exception:
    logging.basicConfig(filename = application_errors_log_file,
                        filemode = 'a',     # "a" stands for appending log messages in the log file, instead of overwritting the file.
                        format = '%(asctime)s - %(levelname)s - %(message)s',
                        level = logging.ERROR)
    logging.exception('')

# Check and log the number of duplicate rows in maintenance records and fleet information
logging.basicConfig(filename = data_quality_log_file,
                    filemode = 'a',
                    format = '%(asctime)s - %(levelname)s - %(message)s',
                    level = logging.INFO)
logging.info(f'Duplicate records found for maintenance data: {duplicated_row_numbers(data_maintenance)}')
logging.info(f'Duplicate records found for fleet data: {duplicated_row_numbers(data_fleet)}')

# Remove the duplicated rows in both dataframe: "data_maintenance", "data_fleet"
data_maintenance_nodup = data_maintenance.drop_duplicates()
data_fleet_nodup = data_fleet.drop_duplicates()

# Identify and log the number of missing data in each column in both dataset
mc_data_fleet = missing_counts(data_fleet_nodup)
logging.info(f'Missing Data in "Fleet Information:"')
for inum in list(range(0, len(mc_data_fleet))):
    logging.info(f'\t{mc_data_fleet.index[inum]}: {mc_data_fleet.values[inum]} missing values')

mc_data_maintenance = missing_counts(data_maintenance_nodup)
logging.info(f'Missing Data in "Maintenance Records:"')
for jnum in list(range(0, len(mc_data_maintenance))):
    logging.info(f'\t{mc_data_maintenance.index[jnum]}: {mc_data_maintenance.values[jnum]} missing values')

# Identify and log the "truck_id" that is presented in the "maintenance" dataset but missing from the "fleet" dataset
logging.info(f'Truck IDs missing from the fleet dataset: {truck_id_maintenances(data_maintenance_nodup, data_fleet_nodup, "truck_id")}')

# Convert the date column in both datasets to a consistent datetime format (YYYY-MM-DD)
data_maintenance_nodup.loc[:, 'maintenance_date'] = pd.Series(date_format(data_maintenance_nodup['maintenance_date']))
data_fleet_nodup.loc[:, 'purchase_date'] = pd.Series(date_format(data_fleet_nodup['purchase_date']))

# Standardize the "service_type" column: lowercase, no whitespaces
data_maintenance_nodup.loc[:, 'service_type'] = data_maintenance_nodup['service_type'].apply(stdize)

# One-hot encoding to the "service_type" column
data_maintenance_encoded = one_hot_encoding(data_maintenance_nodup, ['service_type'])
data_maintenance_encoded.drop('service_type_nan', axis='columns', inplace=True)     # This is to remove the extra encoded column with NaN heading

# Validate the format of technicial email addresses and add a boolean column to show the validity
####### data_maintenance_encoded['email_valid'] = data_maintenance_encoded['technician_email'].apply(email_validity)
data_maintenance_encoded.loc[:, 'email_valid'] = data_maintenance_encoded['technician_email'].apply(email_validity)

# Merge the two datasets ("maintenance" and "fleet") after quality check and processing to export a CSV file
data_merged = merge_df(data_maintenance_encoded, data_fleet_nodup, 'truck_id')
data_merged.loc[:, 'maintenance_cost_per_km'] = maintenance_cost_efficiency(data_merged)
data_final_reordered = data_merged.loc[:, ['truck_id', 'model', 'purchase_date', 'purchase_cost', 'initial_milage', 'milage', 
                                           'maintenance_date', 'maintenance_cost', 'maintenance_cost_per_km', 'service_center', 
                                           'technician_email', 'email_valid', 'service_type_inspection', 'service_type_maintenance', 
                                           'service_type_repair']]   # This is to order the columns for readability
data_final_reordered.to_csv(result_merged_file, index = False)